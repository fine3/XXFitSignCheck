package com.hzy.sign.check;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class PmsHandler implements InvocationHandler {

    protected final Object origin;
    protected final String mPkgName;
    protected final Signature mSign;

    public PmsHandler(Object origin, String pkgName, Signature sign) {
        this.origin = origin;
        this.mPkgName = pkgName;
        this.mSign = sign;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] args) throws Throwable {
        if ("getPackageInfo".equals(method.getName())) {
            String pkgName = (String) args[0];
            Object arg1 = args[1];
            long flag = 0;
            if (arg1 instanceof Long) {
                flag = (long) arg1;
            } else if (arg1 instanceof Integer) {
                flag = (int) arg1;
            }
            if ((flag == PackageManager.GET_SIGNATURES || flag == PackageManager.GET_SIGNING_CERTIFICATES)
                    && mPkgName.equals(pkgName)) {
                PackageInfo info = (PackageInfo) method.invoke(origin, args);
                if (info != null) {
                    if (info.signatures != null && info.signatures.length > 0) {
                        info.signatures[0] = mSign;
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        if (info.signingInfo != null) {
                            Signature[] apkSigns = info.signingInfo.getApkContentsSigners();
                            if (apkSigns != null && apkSigns.length > 0) {
                                apkSigns[0] = mSign;
                            }
                        }
                    }
                }
                return info;
            }
        }
        return method.invoke(origin, args);
    }
}
