package com.hzy.sign.check;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.Signature;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class SignFitness {

    public static boolean fit(Context context, String pkgName, String signStr) {
        return fit(context, pkgName, new Signature(signStr));
    }

    public static boolean fit(Context context, String pkgName, byte[] signBin) {
        return fit(context, pkgName, new Signature(signBin));
    }

    @SuppressLint({"PrivateApi", "DiscouragedPrivateApi"})
    public static boolean fit(Context context, String pkgName, Signature sign) {
        try {
            Class<?> atc = Class.forName("android.app.ActivityThread");
            Method atm = atc.getDeclaredMethod("currentActivityThread");
            Object cat = atm.invoke(null);
            Field pmf = atc.getDeclaredField("sPackageManager");
            pmf.setAccessible(true);
            Object spm = pmf.get(cat);
            Class<?> iPackageManagerInterface =
                    Class.forName("android.content.pm.IPackageManager");
            Object proxy = Proxy.newProxyInstance(iPackageManagerInterface.getClassLoader(),
                    new Class<?>[]{iPackageManagerInterface},
                    new PmsHandler(spm, pkgName, sign));
            pmf.set(cat, proxy);
            PackageManager pm = context.getPackageManager();
            Field mPmField = pm.getClass().getDeclaredField("mPM");
            mPmField.setAccessible(true);
            mPmField.set(pm, proxy);
            return true;
        } catch (Throwable e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            return false;
        }
    }
}
