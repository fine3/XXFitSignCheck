package com.demo.xxfitcheck;

import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ResourceUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.hzy.sign.check.SignFitness;
import com.hzy.xxref.XXSafeRef;


public class MainActivity extends AppCompatActivity {

    private TextView mTxtInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.open_all_ref).setOnClickListener(v -> XXSafeRef.addForAll());
        findViewById(R.id.read_sign_new).setOnClickListener(v -> getSignHash(true));
        findViewById(R.id.read_sign_old).setOnClickListener(v -> getSignHash(false));
        findViewById(R.id.start_sign_fit).setOnClickListener(v -> {
            String chars = ResourceUtils.readAssets2String("sign.txt");
            boolean result = SignFitness.fit(this, getPackageName(), chars);
            ToastUtils.showShort("Fit Sign Check: " + result);
        });
        mTxtInfo = findViewById(R.id.text_sign_info);
    }

    @SuppressLint("SetTextI18n")
    private void getSignHash(boolean isNew) {
        try {
            Signature[] signs;
            if (Build.VERSION.SDK_INT >= 28 && isNew) {
                PackageInfo packageInfo = getPackageManager().getPackageInfo(
                        getPackageName(),
                        PackageManager.GET_SIGNING_CERTIFICATES);
                signs = packageInfo.signingInfo.getApkContentsSigners();
                LogUtils.e("Get Signatures By New");
            } else {
                PackageInfo packageInfo = getPackageManager().getPackageInfo(
                        getPackageName(),
                        PackageManager.GET_SIGNATURES);
                signs = packageInfo.signatures;
                LogUtils.e("Get Signatures By Old");
            }
            if (signs.length > 0) {
                Signature first = signs[0];
                String str = EncryptUtils.encryptSHA1ToString(first.toByteArray());
                ToastUtils.showShort("SIGN: " + str);
                mTxtInfo.setText("SIGN: " + str);
                LogUtils.d(first.toCharsString());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}
